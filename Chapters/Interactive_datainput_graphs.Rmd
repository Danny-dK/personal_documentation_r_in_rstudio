---
author: "D.B. de Koning"
documentclass: article
classoption: twoside
papersize: a4
fontsize: 10pt
geometry: margin=1in
output:
  pdf_document:
    latex_engine: xelatex
header-includes:
- \usepackage{xcolor}
- \usepackage{hyperref}
- \hypersetup{colorlinks = true, linkcolor = black, urlcolor = blue}

- \usepackage{minted}
# - \usemintedstyle{emacs}
- \newmintinline[code]{S}{bgcolor = gray!20!, fontsize = \small}

- \usepackage{fontspec}
- \setmainfont{Georgia}

- \usepackage{setspace}
- \setstretch{1.2}

- \usepackage{fancyhdr}
- \pagestyle{fancy}
- \fancyhead[CO,CE]{}
- \fancyhead[LO,RE]{}
- \fancyhead[LE,RO]{}
- \fancyfoot[CO,CE]{\thepage}
- \fancyfoot[LE, RO]{D.B. de Koning-van Nieuwamerongen 2020}
- \renewcommand{\headrulewidth}{0.5pt}
- \renewcommand{\footrulewidth}{0.5pt}

---



<!-- ```{r, include=FALSE} -->
<!-- options(tinytex.engine_args = '-shell-escape') -->
<!-- ``` -->

<!-- ```{r global_options, include=FALSE} -->
<!-- knitr::opts_chunk$set(fig.pos = 'h',fig.align = 'center') -->
<!-- ``` -->



<!-- this is a R markdown / latex comment. Comments are not printed when knitting to pdf -->

<!-- the line below this one is a guide for the length of the chunk that will fit the width of an A4 margin in the pdf. -->
<!-- ------------------------------------------------------------------------------ -->


# Interactive input

Programming language software such as R can be very daunting as most things have to be typed to get things done instead of clicking on elements. Luckily libraries exists that provide some solutions to some aspects and provide a nice graphical user interface. 



## Interactive data input

One such library is the `DataEditR' library which allows you to create a dataframe or edit data in almost the same manner many may be used to using software such as Excel. For a very nice tutorial see \url{https://cran.r-project.org/web/packages/DataEditR/vignettes/DataEditR.html}.


### New dataframe

To create a new dataframe from scratch:

```{r dataeditR_empty, eval = FALSE, cache = TRUE}
  
  library(DataEditR)
  
  mydata <- data_edit()
  

```

Running the above will open a new screen in which you can enter your data just as you would do with for example Excel. \newline

In the new screen you can click on any of the column headers that start with V and assign your own column name:


```{r column_names, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/columnames.png"))
  
```

If you right click anywhere in the table, the context menu shows up in which you can add or delete columns or rows:


```{r contextmenu, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/contextmenu.png"))
  
```


When all is done, click on synchronize, to synchronize the data to R (specifically to the mydata object).

```{r synchronize, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/synchronize.png"))
  
```


Finally, click on done:

```{r done, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/done.png"))
  
```

Within your environment pane, you will now see the `mydata` object dataframe which you can then use further to work with or analyze. Conversely, if the data could have also been saved as a csv (you will be prompted to browse to your save selection):

```{r save, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/save.png"))
  
```

The underlying code to recreate the dataframe can be saved:

```{r dataeditR_empty2, eval = FALSE, cache = TRUE}
  
  library(DataEditR)
  library(here)

  mydata <- data_edit(code = here("Chapters/Data/data_recreate.R"))
    

```

When running the above code, creating your data, and synchronizing and closing, you will find file `data_recreate.R` in the specified `Data` folder (note that you can call the file whatever you like, but the extension needs to be `.R`). Opening that file shows the code to create the table. This code can be shared with anyone who can run the code and get the same table.


### Edit existing data

To edit a file saved on your computer, the function to read-in the data needs to be supplied (the default function is read.csv). In this case the csv file is ';' separated and therefore it needs the read.csv2 function (or equivalent from the tidyverse):
<!-- ------------------------------------------------------------------------------ -->

```{r dataeditR_empty3, eval = FALSE, cache = TRUE}
  
  library(DataEditR)
  library(here)

  mydata <- data_edit(code = here("Chapters/Data/data_recreate.R"),
                      read_fun = "read.csv2",
                      write_fun = "write.csv2")
    

```

When running the above code, the same empty dataframe opens. Now click on 'Browse' and select the `animaldata.csv` in the folder `Chapters/Data/`.

```{r browseupload, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/browse.png"))
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/uploaded.png"))
  
```

The imported data can then be edited and saved again. Note that above a write function was added to tell DataEditR how to save the data. \newline

If existing data is present within your R environment, just replace the word template with the name of the object in the environment.

```{r mydata, echo = FALSE, out.width = '75%', message = FALSE}

  library(knitr)
  library(here)
  include_graphics(here("Chapters/Rmarkdown_pics/dataeditr/mydata.png"))

```


\newpage

## Interactive graph creation 

(To be added soon, using the "esquisse" package. https://github.com/dreamRs/esquisse )

\newpage