## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>

All content in this repository with the exception of the chapters "R Basics" and "Plotting Graphs" is licensed under [Creative Commons Attribution 4.0
International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/). Citation for the chapters "R Basics" and "Plotting Graphs" is mentioned in the documentation.

Note that you are responsible yourselve for the things that you apply in your own work. The author is not responsible or liable for any 'damages' that might result from the usage of these functions.
