# R-and-Rstudio-self-documentation
Documentation of R and Rstudio functions to be used as a reference guide of 
general functions. 

## Requirements
This is all done on Windows 10, although most will work on other OS, can't guarentee it.
- Minimum version of R is 4.1.2.
- Minimum version of Rstudio is 2021.09.1.
- Python (for the pygments package) 3.8 (only required if you will render the enclosed
Rmarkdown documents).
- R packages see chapter 'Packages and R'.

## Usage
Please download all files as a zip and unpack locally on your computer.
You can do so by clicking on the download button which is next to the 'clone' 
button on this page (the blue button above).

Among the downloaded files is a pdf file which is the main document that you
can use as a reference.

You most likely will need to install libraries to use the codes in the pdf. If
you are not familiar with R, at least read chapter Packages and R first.

If you want to try out some of the code chunks in the pdf, first open the 
'R_functions_markdown.Rproj' (by double clicking) and then open 'your_practise_file.R'
in which you can paste the codes you copied from the pdf. You don't need to worry
about setting the work directory, as the code chunks will use the 'here' library to 
refer to the correct folders automatically (as long as you haven't hardcoded the
location of the work directory in any other system file). 

I'm starting to make reprex (reproducible example) for each chunk you see. This 
will take some time. Check back regularly for updates (I will try to update monthly, 
but time is limited). Thus for now, not all code chunks are able to be run.

## Rmarkdown users
Feel free to  open the Rmd files. You can run the code chunks directly from the Rmd 
files as well. The Rmd files are used to knit to pdf (the pdf that you donwloaded). 
Knitting to pdf is done using the TinyTex package in R. Check the YAML header to see which packages are
invoked. If missing, TinyTex should be able to donwload them automatically. If you want to
knit to pdf, be aware that the minted package is required which in turn requires the Python package
Pygments (install in an elevated cmd prompt with: pip install Pygments).

I specifically do not use the bookdown package as I could not get it to knit my documents
properly and preview individual chapters (my guess is mainly because of the flag option 
required just below the yaml, and to individually knit chapters I need to uncomment that 
section in each document to properly knit that chapter).

## Disclaimers 
When I started learning R, I created this document initially 
for myself, which I thought would be beneficial for me as a reference guide. 
I am not an experienced scripter and my memory is not the best ever, so I
generally require an aid to look up how I did certain things in my work. I
noted however that others may have some use out of this like I did. I know that
when I started learning R I would have loved a document like this to help me
on my way, instead of figuring it out myself using many online searches (and
an incredible amount of frustration and cursing). 

Because I did this initially for myself, I could not be bothered by citing all
the solutions to my R problems that I could find on StackOverflow or other online
open source sources, hence the lack of citations (and I probably won't be adding them). 

As said before, I am not an experienced scripter. Could more advanced R users
think of better more efficient ways to get things done... probably.
Could more advanced users find prettier ways of coding and using Rmarkdown....
probably. This is in no way an official guide and I
am not liable for any 'damages' resulting from the use of these functions. It has 
been created with the best intentions. I keep learning and finding new things in 
R, I keep improving and learn how to do things better. 

Yes, I know that you can look up or search online on how to do things in R, but
if you don't know where to start, it can be quite difficult to find what you need.
In addition, I don't always find the help pages of R really intuitive.

Repeating myself again: I created this for myself. Things might seem cryptic to you
but are familiar to me as I had to use it in my own work while learning R from the 
beginning.

